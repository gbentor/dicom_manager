import pydicom
import sys
import os
import tarfile
import tkinter as tk
from tkinter import messagebox
import urllib.request
from shutil import move, copyfileobj, rmtree
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine, MetaData, Table, Column, Integer, CHAR, String
from sqlalchemy.sql import func


class DicomManager:
    """
    Manager to handle a set of DICOM data.
    Support the arrangement of the data, and executing several queries.
    """
    def __init__(self):
        """
        On instantiation create a Data directory to keep all data files.
        Initialize two SQLite Dbs to hold data on patients and scans.
        """
        self.data_path = os.path.join(os.getcwd(), 'Data')
        os.makedirs(self.data_path, exist_ok=True)
        self.__create_new_db()

    def arrange_data(self, file_path: str):
        """
        Takes a path to a tgz file.
        Extract it to temp dir, and for each file - move it to desired dir and add relevant data to DBs.
        :param file_path: path to a tgz file
        """
        # Extract the tgz file and remove it afterwards
        path_to_files = os.path.dirname(file_path)
        with tarfile.open(file_path, "r:gz") as tar:
            tar.extractall(path_to_files)
        os.remove(file_path)

        # get connection and session of both DBs
        user_conn, users_session = self.__get_db_conn_session(self.users_eng)
        scans_conn, scans_session = self.__get_db_conn_session(self.scans_eng)

        id_count = scans_session.query(self.scans_db.c.ScanID).count()  # We give the scans a unique numeric id
        for file in os.listdir(path_to_files):
            ds = pydicom.dcmread(os.path.join(path_to_files, file))  # get the DICOM file data

            file_path = self.__move_file_to_location(ds, file, path_to_files)  # move file to final location
            self.__add_user_to_db(ds, user_conn, users_session)  # add the user data to th DB
            id_count = self.__add_scan_to_db(ds, file_path, id_count, scans_conn)  # add the scan data to DB

        rmtree(path_to_files)  # remove temp directory

    def get_users_data(self) -> list:
        """
        :return: Returns a list of patients, their age and sex
        """
        user_conn, users_session = self.__get_db_conn_session(self.users_eng)
        return users_session.query(self.users_db.c.PatientName, self.users_db.c.PatientAge, self.users_db.c.PatientSex).all()

    def get_avg_scan_time(self) -> float:
        """
        Return the average time of the scans
        :return: avg time of scans
        """
        conn, session = self.__get_db_conn_session(self.scans_eng)
        qry = session.query(func.avg(self.scans_db.c.ExposureTime).label("ExposureTime"))
        return qry.first()[0]

    def get_unique_hospitals_count(self) -> int:
        """
        Return the number of hospitals in the data
        :return: number of hospitals
        """
        conn, session = self.__get_db_conn_session(self.scans_eng)
        return session.query(self.scans_db.c.InstitutionName).distinct().count()

    def __create_new_db(self):
        """
        Create new SQL DBs, one for the users and another for scans
        """
        if not self.__is_file_exists('Data/users.db') or not self.__is_file_exists('Data/scans.db'):
            exit()
        self.users_eng = create_engine('sqlite:///Data/users.db', echo=False)
        meta = MetaData()
        self.users_db = Table(
            'users', meta,
            Column('PatientName', String, primary_key=True),
            Column('PatientAge', Integer),
            Column('PatientSex', CHAR)
        )
        meta.create_all(self.users_eng)

        self.scans_eng = create_engine('sqlite:///Data/scans.db', echo=False)
        meta = MetaData()
        self.scans_db = Table(
            'scans', meta,
            Column('ScanID', Integer, primary_key=True),
            Column('PatientName', String),
            Column('InstitutionName', String),
            Column('ExposureTime', Integer),
            Column('FilePath', String)
        )
        meta.create_all(self.scans_eng)

    def __move_file_to_location(self, ds, file: str, path_to_files: str) -> str:
        """
        Move a given file to its location.
        Create the directory tree as needed - patient/study/series
        :param ds: file data
        :param file: file name
        :param path_to_files: destination of files
        :return: path of file detination
        """
        patient_path = os.path.join(self.data_path, f"{ds.PatientID}-{ds.PatientName}")
        os.makedirs(patient_path, exist_ok=True)

        study_path = os.path.join(patient_path, ds.StudyInstanceUID)
        os.makedirs(study_path, exist_ok=True)

        series_path = os.path.join(study_path, ds.SeriesInstanceUID)
        os.makedirs(series_path, exist_ok=True)

        move(os.path.join(path_to_files, file), os.path.join(series_path,file))

        return os.path.join(series_path, file)

    def __add_user_to_db(self, ds, user_conn, users_session):
        """
        Add a new user to DB.
        We assume the patient name is unique and so it can be used as the primary key.
        :param ds: file data
        :param user_conn: db connection
        :param users_session: db session
        """
        if not users_session.query(self.users_db.c.PatientName).filter(self.users_db.c.PatientName == str(ds.PatientName)).count():
            ins = self.users_db.insert().values(PatientName=str(ds.PatientName),
                                                PatientAge=int(ds.PatientAge[:-1]),
                                                PatientSex=ds.PatientSex)
            user_conn.execute(ins)

    def __add_scan_to_db(self, ds, file_path, id_count, scans_conn):
        """
        Add a new scan to DB
        :param ds: file data
        :param file_path: path of file
        :param id_count: the id to give to the scan
        :param scans_conn: db connection
        :return: updated scans count
        """
        inst = self.scans_db.insert().values(ScanID=id_count,
                                             PatientName=str(ds.PatientName),
                                             InstitutionName=str(ds.InstitutionName),
                                             ExposureTime=int(ds.ExposureTime),
                                             FilePath=file_path)
        scans_conn.execute(inst)
        return id_count + 1

    @classmethod
    def __get_db_conn_session(cls, engine):
        """
        Create new connection and session
        :param engine: db engine
        :return: connection, session
        """
        user_conn = engine.connect()
        session = sessionmaker(bind=engine)
        users_session = session()
        return user_conn, users_session

    @classmethod
    def __is_file_exists(cls, file):
        if os.path.exists(file):
            root = tk.Tk()
            answer = messagebox.askquestion('DB already exists', 'DB File already exists!\nOK to proceed?', icon='warning')
            root.destroy()
            return answer == 'yes'
        return True


if __name__ == "__main__":
    if len(sys.argv) != 2:
        raise Exception("Wrong number of arguments! Single URL is expected")

    os.makedirs(os.path.join('temp_data', 'data_file'), exist_ok=True)
    with urllib.request.urlopen(sys.argv[1]) as response, open(os.path.join('temp_data', 'data_file', 'data_file.tgz'), 'wb') as out_file:
        copyfileobj(response, out_file)

    dicom = DicomManager()
    dicom.arrange_data(os.path.join(os.getcwd(), 'temp_data', 'data_file', 'data_file.tgz'))
    print(dicom.get_users_data())
    print(dicom.get_avg_scan_time())
    print(dicom.get_unique_hospitals_count())
